// TODO: Unsupported Syntax
// - [ ] Encoding conversion (3.4.5 Unicode Strings; __?utf16be?__)
// - [ ] Special float constants (3.4.6 Floating-Point Constants; __?float64?__; __?NaN?__)
// - [ ] Float literals in non decimal bases (3.4.6 Floating-Point Constants; 0x1p+2)
// - [ ] Effective addresses (3.3 Effective Addresses) FIXME
// - [ ] Proper highlightung for macros (4. The NASM Preprocessor)
// - [ ] Float, BCD tests in corpus/consts.nasm.txt
// - [ ] Expr tests
// - [ ] Comment tests
// - [ ] Make remaining /i rules truly case insensitive
// - [ ] Instruction Prefixes
// - [ ] explicit operand sizes (e.g. push dword 0)

module.exports = grammar({
    name: "nasm",
    supertypes: $ => [
        $._mnemonic,
        $._literal,
        $._string,
    ],
    extras: $ => [$._whitespace],
    rules: {
        source_file: $ => seq(
            repeat(choice(
                $._linebreak,
                seq($.inst, $._linebreak),
                $._label_def_outer
            )),
            optional(choice($.inst, $.label_def))
        ),

        _linebreak: $ => choice(seq($.line_comment, $.newline), $.newline),

        // Whitespace containing at least one newline with optional comment
        newline: $ => /[r\t\f ]*\n[\r\t\f ]*/,

        // Line comment starting with //
        line_comment: $ => /;[^\n]*/,

        // Simple whitespace (No newlines except escaped ones)
        _whitespace: $ => /(([\t\f ]+)|(([\t\f ]*\\[\t\f ]*\n[\t\f ]*)))+/,

        // ==== Instructions ====
        // Public and local label definitions, see: https://www.nasm.us/doc/nasmdoc3.html#section-3.1
        inst: $ => seq($._mnemonic, optional(seq($._whitespace, $._expr, repeat(seq(",", $._expr))))),
        _mnemonic: $ => choice($.op_mnemonic, $.macro_mnemonic, $.pseudo_mnemonic, $.directive_mnemonic),

        // All IA64 opcodes, see: https://www.nasm.us/doc/nasmdocb.html#section-B.1.1
        op_mnemonic: $ => read_enum("insns.txt"),

        // Preprocessor mnemonics, see: https://github.com/netwide-assembler/nasm/blob/master/x86/insns.dat
        macro_mnemonic: $ => /%(\w|[-_])+/,

        // Pseudo mnemonics, see: https://www.nasm.us/doc/nasmdoc3.html#section-3.2
        pseudo_mnemonic: $ => /((d|res)[bhwdqtoyz])|incbin|equ|times/i,

        // Directive mnemonics, see: https://www.nasm.us/doc/nasmdoc7.html#section-7
        directive_mnemonic: $ => /bits|use(16|32)|default|section|segment|absolute|extern|required|global|common|static|(g|l)(pre|post)fix|cpu|float/i,

        // ==== Literals ====
        _literal: $ => choice($.integer, $._string, $.float, $.bcd, $.register, $.pub_label, $.local_label, $.pseudo_label),

        // Register names, see: https://www.nasm.us/doc/nasmdo12.html#section-12.1
        // and https://github.com/netwide-assembler/nasm/blob/master/x86/regs.dat
        register: $ => read_enum("regs.txt"),

        // Integer literals, see function below
        integer: $ => integer_literals(),

        // Any string, see: https://www.nasm.us/doc/nasmdoc3.html#section-3.4.2
        _string: $ => choice($.verbatim_string, $.backquoted_string),

        // Verbatim string, see: https://www.nasm.us/doc/nasmdoc3.html#section-3.4.2
        verbatim_string: $ => /("[^"\n]*")|('[^\\n']*')/,

        // Backquoted string, see: https://www.nasm.us/doc/nasmdoc3.html#section-3.4.2
        backquoted_string: $ => seq(
            "`",
            repeat(choice(
                token.immediate(prec(1, /[^`\n\\]+/)),
                $.c_escape_seq
            )),
            "`"
        ),

        // valid single letters after \ are: ['"`\\?abtnvfe]
        c_escape_seq: $ => token.immediate(
            /\\([^0-9xuU]|([0-7][0-7]?[0-7]?)|(x[0-9a-fA-F][0-9a-fA-F]?)|(u[0-9a-fA-F]{4})|(U[0-9a-fA-F]{8}))/
        ),


        // Float literals, see: https://www.nasm.us/doc/nasmdoc3.html#section-3.4.6
        float: $ => /[+-]?[0-9]+\.[0-9]*([eE]][0-9]+)?/,

        // BCD literals, see: https://www.nasm.us/doc/nasmdoc3.html#section-3.4.7
        bcd: $ => /(0p[0-9]+)|[0-9]+p/,

        // ==== Expressions ====
        // Expression, see: https://www.nasm.us/doc/nasmdoc3.html#section-3.5
        _expr: $ => choice($._literal, prec(1, $.conditional), prec(2, $.bin_op), prec(3, $.un_op)),

        bin_op: $ => choice.apply(null, bin_ops($)),
        un_op: $ => choice.apply(null, un_ops($)),

        // Conditional operator, see: https://www.nasm.us/doc/nasmdoc3.html#section-3.5.1
        conditional: $ => prec.right(1, seq($._expr, "?", $._expr, ":", $._expr)),

        // ==== Labels ====
        _label_def_outer: $ => seq($.label_def, choice(
                $._linebreak,
                $._whitespace,
                ":",
        )),
        label_def: $ => choice($.pub_label, $.local_label),
        pub_label: $ =>      /([A-Za-z_?])(\w|[._$#@~?])+/,
        local_label: $ => /\.([.A-Za-z_?])(\w|[._$#@~?])+/,
        pseudo_label: $ => /\$+/,
    }
});

// Number literals, see https://www.nasm.us/doc/nasmdoc3.html#section-3.4.1
function integer_literals() {
    const digits = "0123456789abcdef";

    bases = {
        2: "by",
        8: "qo",
        10: "dt",
        16: "hx",
    };

    s = "";
    for (let base in bases) {
        let base_chars = "[" + bases[base]+ bases[base].toUpperCase() + "]";
        let base_digits = "[_" + digits.slice(0, base) + digits.slice(10, base).toUpperCase() + "]+";
        s += "(0" + base_chars + base_digits + ")|(" + base_digits + base_chars + ")|";
    }
    s += "([0-9]+)|(\\$[0-9][0-9a-fA-F]+)";
    return RegExp(s);
}

function bin_ops($) {
    let bin_ops = ["||", "^^", "&&", "=", "==", "!=", "<>", "<", "<=", ">", ">=", "<=>", "|", "^", "<<", ">>", "<<<", ">>>", "+", "-", "*", "/", "//", "%", "%%"];
    let grammar = [];

    for (let i = 0; i < bin_ops.length; i++) {
        grammar.push(prec.left(i, seq($._expr, bin_ops[i], $._expr)));
    }

    return grammar;
}

function un_ops($) {
    let un_ops = ["-", "+", "~", "!", /[sS][eE][gG]/]
    let grammar = [];

    for (let i = 0; i < un_ops.length; i++) {
        grammar.push(prec(i+100, seq(un_ops[i], $._expr)));
    }

    return grammar;
}

function read_enum(path) {
    const fs = require("fs");
    const f = fs.readFileSync("data/" + path, "utf-8");
    let regex = "";

    f.split(/\r?\n/).forEach(l => {
        if (l === "")
            return;
        regex += "|(";
        l.split("").forEach(c => {
            regex += "[" + c.toUpperCase() + c.toLowerCase() + "]"
        });
        regex += ")";
    });

    return new RegExp(regex.substring(1));
}
