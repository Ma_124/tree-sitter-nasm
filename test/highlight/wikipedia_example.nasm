; SYNTAX TEST
global _start
; <- keyword.directive.nasm
;^^^^^ keyword.directive.nasm
;      ^^^^^^ function.label.pub.nasm

section .text
; <- keyword.directive.nasm
;^^^^^^ keyword.directive.nasm
;       ^^^^^ function.label.local.nasm

_start:
; <- function.label.pub.nasm
;^^^^^ function.label.pub.nasm

    mov eax, 4 ; write
;   ^^^ function.builtin.instruction.nasm
;       ^^^ variable.builtin.register.nasm
;            ^ constant.integer.nasm
;              ^^^^^^^ comment.line.nasm

    mov ebx, 1 ; stdout
;   ^^^ function.builtin.instruction.nasm
;       ^^^ variable.builtin.register.nasm
;            ^ constant.integer.nasm
;              ^^^^^^^^ comment.line.nasm

    mov ecx, msg
;   ^^^ function.builtin.instruction.nasm
;       ^^^ variable.builtin.register.nasm
;            ^^^ function.label.pub.nasm

    mov edx, msg.len
;   ^^^ function.builtin.instruction.nasm
;       ^^^ variable.builtin.register.nasm
;            ^^^^^^^ function.label.pub.nasm

    int 0x80   ; write(stdout, msg, strlen(msg));
;   ^^^ function.builtin.instruction.nasm
;       ^^^^ constant.integer.nasm
;              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ comment.line.nasm

    xor eax, msg.len ; invert return value from write()
;   ^^^ function.builtin.instruction.nasm
;       ^^^ variable.builtin.register.nasm
;            ^^^^^^^ function.label.pub.nasm
;                    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ comment.line.nasm

    xchg eax, ebx ; value for exit()
;   ^^^^ function.builtin.instruction.nasm
;        ^^^ variable.builtin.register.nasm
;             ^^^ variable.builtin.register.nasm
;                 ^^^^^^^^^^^^^^^^^^ comment.line.nasm

    mov eax, 1 ; exit
;   ^^^ function.builtin.instruction.nasm
;       ^^^ variable.builtin.register.nasm
;            ^ constant.integer.nasm
;              ^^^^^^ comment.line.nasm

    int 0x80   ; exit(...)
;   ^^^ function.builtin.instruction.nasm
;       ^^^^ constant.integer.nasm
;              ^^^^^^^^^^^ comment.line.nasm

section .data
; <- keyword.directive.nasm
;^^^^^^ keyword.directive.nasm
;       ^^^^^ function.label.local.nasm

msg:    db     "Hello, world!", 10
; <- function.label.pub.nasm
;^^ function.label.pub.nasm
;       ^^ keyword.pseudo.nasm
;              ^^^^^^^^^^^^^^^ string.raw.nasm
;                               ^^ constant.integer.nasm

.len:   equ    $ - msg
; <- function.label.local.nasm
;^^^ function.label.local.nasm
;       ^^^ keyword.pseudo.nasm
;              ^ function.builtin.label.pseudo.nasm
;                  ^^^ function.label.pub.nasm
