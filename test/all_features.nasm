%ifndef ALL_FEATURES_ASM
%define ALL_FEATURES_ASM
section .text

;; Entrypoint
_start:
    ; Call write
    mov rax, 1
    mov rdi, 1
    mov rsi, hello_world
    mov rdx, hello_world_len
    syscall

    ; Load 6th char of hello_world
    mov rax, 6
    mov rbx, [hello_world + rax * 1]

    ; Call exit
    mov	rax, qword 60
    xor rdi, rdi
    syscall

.spin:
    pause
    jmp .spin

section .data
hello_world:     db `Hello World\n`
hello_world_len: equ $ - hello_world

some_ints: dw 10, \
              0o10, \
              10h

some_floats:  dw    -1.0, __?NaN?__
some_bcd:     dw    0p1986
some_bcd_seg: equ   seg some_bcd

is_64_bit: db  __?BITS?__ == 64 ? 1 : 0
true:      equ 1 == 1
false:     equ !(1 == 1)

%endif
