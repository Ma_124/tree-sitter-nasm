.PHONY: build generate check clean

PATH  := $(PATH):$(PWD)/node_modules/.bin

build: generate
	$(CC) -shared -fPIC -g -O2 -I src -xc src/parser.c -o bin/nasm.so

generate:
	tree-sitter generate

check:
	tree-sitter test

clean:
	rm -rf bin/* bindings binding.gyp build src tree-sitter-nasm.wasm

