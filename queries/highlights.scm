(line_comment) @comment
(op_mnemonic) @function.builtin
(macro_mnemonic) @function.macro
(pseudo_mnemonic) @function.special
(directive_mnemonic) @function.special

(register) @variable.builtin
(integer) @number
(verbatim_string) @string
(backquoted_string) @string.special
(c_escape_seq) @escape
(float) @number
(bcd) @number

; TODO bin_op, un_op, conditional
; TODO treat '.text' specially in 'section .text'

(pub_label) @function
(local_label) @function
(pseudo_label) @function
