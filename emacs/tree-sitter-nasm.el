;;; tree-sitter-nasm.el ---- NASM grammar for tree-sitter -*- lexical-binding: t; coding: utf-8 -*-

;; Copyright (C) 2021 Ma_124 <ma_124+oss@pm.me>
;;
;; Author: Ma_124 <ma_124+oss@pm.me>
;; Keywords: language tool parser tree-sitter assembly nasm
;; Homepage: https://gitlab.com/Ma_124/tree-sitter-nasm/-/tree/master/emacs
;; Version: 0.1.0
;; Package-Requires: ((emacs "25.1") (tree-sitter "0.15.0"))
;; SPDX-License-Identifier: MIT

;;; Commentary:

;; Includes a NASM grammar for `tree-sitter' which can be used for better syntax highlighting.

;;; Code:

(require 'cl-lib)
(require 'tree-sitter)
(require 'tree-sitter-load)
(require 'tree-sitter-hl)

(defconst tree-sitter-nasm--root-dir
  (file-name-directory                         ; get parent directory with trailing slash
   (directory-file-name                        ; trim trailing slash
    (file-name-directory                       ; get parent directory with trailing slash
     (locate-library "tree-sitter-nasm.el")))) ; get name of .el file
  "The root directory of tree-sitter-nasm (contains grammar.js).")

(defvar tree-sitter-nasm--bin-dir
  (file-name-as-directory (concat tree-sitter-nasm--root-dir "bin"))
  "Path to the directory containing the compiled file.")

(defvar tree-sitter-nasm--queries-dir
  (file-name-as-directory (concat tree-sitter-nasm--root-dir "queries"))
  "Path to the directory containing queries.")

(defun tree-sitter-nasm--highlight-queries ()
  "Return path to the `highlights.scm' queries file."
  (concat tree-sitter-nasm--queries-dir "highlights.scm"))

(defun tree-sitter-nasm-ensure ()
  "Return the NASM language object."
  (unwind-protect
      (condition-case nil
          (tree-sitter-require 'nasm)
        (error (display-warning
                'tree-sitter-nasm
                "Could not load grammar for nasm.")))
    (tree-sitter-nasm-copy-queries)))

;;;###autoload
(defun tree-sitter-nasm--init-load-path (&rest _args)
  "Add the directory containing the compiled grammar to `tree-sitter-load-path'."
  (cl-pushnew tree-sitter-nasm--bin-dir tree-sitter-load-path
              :test #'string-equal)
  (advice-remove 'tree-sitter-load #'tree-sitter-nasm--init-load-path))

;;;###autoload
(advice-add 'tree-sitter-load :before #'tree-sitter-nasm--init-load-path)

;;;###autoload
(defun tree-sitter-nasm--init-major-mode-alist (&rest _args)
  "Link major mode to grammar."
  (cl-pushnew '(nasm-mode . nasm) tree-sitter-major-mode-language-alist :key #'car)
  (advice-remove 'tree-sitter--setup #'tree-sitter-nasm--init-major-mode-alist))

;;;###autoload
(advice-add 'tree-sitter--setup :before #'tree-sitter-nasm--init-major-mode-alist)
;;; Normal load.
(tree-sitter-nasm--init-major-mode-alist)

(defun tree-sitter-nasm--hl-default-patterns (&rest _args)
  "Zse syntax highlighting patterns."
  (unless tree-sitter-hl-default-patterns
    (setq tree-sitter-hl-default-patterns (with-temp-buffer
                                            (insert-file-contents (tree-sitter-nasm--highlight-queries))
                                            (buffer-string)))))

;;;###autoload
(advice-add 'tree-sitter-hl--setup :before
            #'tree-sitter-nasm--hl-default-patterns)

(provide 'tree-sitter-nasm)

;;; tree-sitter-nasm.el ends here
