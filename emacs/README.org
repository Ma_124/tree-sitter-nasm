#+TITLE: Emacs Minor Mode for NASM
** Overview
This directory contains a minor mode providing NASM support for [[https://emacs-tree-sitter.github.io][Emacs tree-sitter]].

As a major mode you can use [[https://github.com/skeeto/nasm-mode][nasm-mode]] by skeeto.
Another useful minor mode is [[https://github.com/skeeto/x86-lookup][x86-lookup]], also by skeeto.

** Installation
Download and compile grammar:
#+begin_src bash
git clone "https://gitlab.com/Ma_124/tree-sitter-nasm.git"
cd tree-sitter-nasm
npm i
make
#+end_src

Load package from your emacs config:
#+begin_src elisp
(add-to-list 'load-path "/.../tree-sitter-nasm/emacs")
(require 'tree-sitter-nasm)
#+end_src

** Dependencies
- C Compiler
- Node.js and NPM
